package com.jkg.Pojo;

/**
 *
 * @author jeangatt
 */
public enum RestaurantReviewsWebsite {
    TRIPADIVISOR,
    YELP
}
