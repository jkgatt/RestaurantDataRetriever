package com.jkg.Pojo;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author jeangatt
 */
public class Restaurants {

    @SerializedName("version")
    private int version;
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    private RestaurantsData response;

    public Restaurants(int version, String status, RestaurantsData response) {
        this.version = version;
        this.status = status;
        this.response = response;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public RestaurantsData getResponse() {
        return response;
    }

    public void setResponse(RestaurantsData response) {
        this.response = response;
    }    
}
