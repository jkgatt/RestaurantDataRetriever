package com.jkg.Pojo;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 *
 * @author jeangatt
 */
public class RestaurantsData {
    @SerializedName("restaurants")
    private ArrayList<Restaurant> restaurantList;
    @SerializedName("restaurant_count")
    private int restaurantCount;
    @SerializedName("total_review_count")
    private int totalReviewCount;

    public RestaurantsData(ArrayList<Restaurant> restaurantList, int restaurantCount, int totalReviewCount) {
        this.restaurantList = restaurantList;
        this.restaurantCount = restaurantCount;
        this.totalReviewCount = totalReviewCount;
    }

    public ArrayList<Restaurant> getRestaurantList() {
        return restaurantList;
    }

    public void setRestaurantList(ArrayList<Restaurant> restaurantList) {
        this.restaurantList = restaurantList;
    }

    public int getRestaurantCount() {
        return restaurantCount;
    }

    public void setRestaurantCount(int restaurantCount) {
        this.restaurantCount = restaurantCount;
    }

    public int getTotalReviewCount() {
        return totalReviewCount;
    }

    public void setTotalReviewCount(int totalReviewCount) {
        this.totalReviewCount = totalReviewCount;
    }
}
