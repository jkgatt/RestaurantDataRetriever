package com.jkg.Pojo;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

/**
 *
 * @author jeangatt
 */
public class RestaurantReview {

    @SerializedName("review_website")
    private RestaurantReviewsWebsite reviewWebsite;
    @SerializedName("review_url")
    private String reviewUrl;
    @SerializedName("review_title")
    private String reviewTitle;
    @SerializedName("review_text")
    private String reviewText;
    @SerializedName("review_rating")
    private int reviewRating;
    @SerializedName("review_date")
    private Date reviewDate;

    public RestaurantReview(RestaurantReviewsWebsite reviewWebsite, String reviewUrl, String reviewTitle, String reviewText, int reviewRating, Date reviewDate) {
        this.reviewWebsite = reviewWebsite;
        this.reviewUrl = reviewUrl;
        this.reviewTitle = reviewTitle;
        this.reviewText = reviewText;
        this.reviewRating = reviewRating;
        this.reviewDate = reviewDate;
    }

    public RestaurantReviewsWebsite getReviewWebsite() {
        return reviewWebsite;
    }

    public void setReviewWebsite(RestaurantReviewsWebsite reviewWebsite) {
        this.reviewWebsite = reviewWebsite;
    }

    public String getReviewUrl() {
        return reviewUrl;
    }

    public void setReviewUrl(String reviewUrl) {
        this.reviewUrl = reviewUrl;
    }

    public String getReviewTitle() {
        return reviewTitle;
    }

    public void setReviewTitle(String reviewTitle) {
        this.reviewTitle = reviewTitle;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public int getReviewRating() {
        return reviewRating;
    }

    public void setReviewRating(int reviewRating) {
        this.reviewRating = reviewRating;
    }

    public Date getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    } 
}
