package com.jkg.Pojo;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 *
 * @author jeangatt
 */
public class RestaurantsDays {
    @SerializedName("monday")
    private ArrayList<ArrayList<String>> monday;
    @SerializedName("tuesday")
    private ArrayList<ArrayList<String>> tuesday;
    @SerializedName("wednesday")
    private ArrayList<ArrayList<String>> wednesday;
    @SerializedName("thursday")
    private ArrayList<ArrayList<String>> thursday;
    @SerializedName("friday")
    private ArrayList<ArrayList<String>> friday;
    @SerializedName("saturday")
    private ArrayList<ArrayList<String>> saturday;
    @SerializedName("sunday")
    private ArrayList<ArrayList<String>> sunday;

    public RestaurantsDays(ArrayList<ArrayList<String>> monday, 
            ArrayList<ArrayList<String>> tuesday, 
            ArrayList<ArrayList<String>> wednesday, 
            ArrayList<ArrayList<String>> thursday, 
            ArrayList<ArrayList<String>> friday, 
            ArrayList<ArrayList<String>> saturday, 
            ArrayList<ArrayList<String>> sunday) {
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
    }

    public ArrayList<ArrayList<String>> getMonday() {
        return monday;
    }

    public void setMonday(ArrayList<ArrayList<String>> monday) {
        this.monday = monday;
    }

    public ArrayList<ArrayList<String>> getTuesday() {
        return tuesday;
    }

    public void setTuesday(ArrayList<ArrayList<String>> tuesday) {
        this.tuesday = tuesday;
    }

    public ArrayList<ArrayList<String>> getWednesday() {
        return wednesday;
    }

    public void setWednesday(ArrayList<ArrayList<String>> wednesday) {
        this.wednesday = wednesday;
    }

    public ArrayList<ArrayList<String>> getThursday() {
        return thursday;
    }

    public void setThursday(ArrayList<ArrayList<String>> thursday) {
        this.thursday = thursday;
    }

    public ArrayList<ArrayList<String>> getFriday() {
        return friday;
    }

    public void setFriday(ArrayList<ArrayList<String>> friday) {
        this.friday = friday;
    }

    public ArrayList<ArrayList<String>> getSaturday() {
        return saturday;
    }

    public void setSaturday(ArrayList<ArrayList<String>> saturday) {
        this.saturday = saturday;
    }

    public ArrayList<ArrayList<String>> getSunday() {
        return sunday;
    }

    public void setSunday(ArrayList<ArrayList<String>> sunday) {
        this.sunday = sunday;
    }
    
    
}
