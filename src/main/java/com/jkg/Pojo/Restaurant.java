package com.jkg.Pojo;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/**
 *
 * @author jeangatt
 */
public class Restaurant {

    @SerializedName("name")
    private String name;
    @SerializedName("address")
    private String address;
    @SerializedName("address_extended")
    private String addressExtended;
    @SerializedName("locality")
    private String locality;
    @SerializedName("region")
    private String region;
    @SerializedName("country")
    private String country;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("tel")
    private String telephone;
    @SerializedName("fax")
    private String fax;
    @SerializedName("chain_name")
    private String chainName;
    @SerializedName("website")
    private String website;
    @SerializedName("email")
    private String email;
    @SerializedName("cuisine")
    private ArrayList<String> cuisine;
    @SerializedName("price")
    private String price;
    @SerializedName("rating")
    private double rating;
    @SerializedName("hours")
    private RestaurantsDays hours;
    @SerializedName("parking")
    private boolean parking;
    @SerializedName("parking_valet")
    private boolean parkingValet;
    @SerializedName("parking_garage")
    private boolean parkingGarage;
    @SerializedName("parking_street")
    private boolean parkingStreet;
    @SerializedName("parking_lot")
    private boolean parkingLot;
    @SerializedName("parking_validated")
    private boolean parkingValidated;
    @SerializedName("parking_free")
    private boolean parkingFree;
    @SerializedName("smoking")
    private boolean smoking;
    @SerializedName("accessible_wheelchair")
    private boolean wheelchairAccessible;
    @SerializedName("wifi")
    private boolean wifi;
    @SerializedName("meal_breakfast")
    private boolean mealBreakfast;
    @SerializedName("meal_deliver")
    private boolean mealDeliver;
    @SerializedName("meal_dinner")
    private boolean mealDinner;
    @SerializedName("meal_lunch")
    private boolean mealLunch;
    @SerializedName("meal_takeout")
    private boolean mealTakeout;
    @SerializedName("meal_cater")
    private boolean mealCater;
    @SerializedName("options_healthy")
    private boolean healthyOptions;
    @SerializedName("options_organic")
    private boolean organicOptions;
    @SerializedName("options_vegetarian")
    private boolean vegetarianOptions;
    @SerializedName("options_vegan")
    private boolean veganOptions;
    @SerializedName("options_glutenfree")
    private boolean glutenFreeOptions;
    @SerializedName("options_lowfat")
    private boolean lowFatOptions;
    @SerializedName("reviews")
    private ArrayList<RestaurantReview> reviews;
    @SerializedName("trip_advisor_url")
    private String tripAdvisorURL;
    @SerializedName("yelp_url")
    private String yelpUrl;

    public Restaurant(String name, String address, String addressExtended, String locality, String region, String country, double latitude, double longitude, String telephone, String fax, String chainName, String website, String email, ArrayList<String> cuisine, String price, double rating, RestaurantsDays hours, boolean parking, boolean parkingValet, boolean parkingGarage, boolean parkingStreet, boolean parkingLot, boolean parkingValidated, boolean parkingFree, boolean smoking, boolean wheelchairAccessible, boolean wifi, boolean mealBreakfast, boolean mealDeliver, boolean mealDinner, boolean mealLunch, boolean mealTakeout, boolean mealCater, boolean healthyOptions, boolean organicOptions, boolean vegetarianOptions, boolean veganOptions, boolean glutenFreeOptions, boolean lowFatOptions, ArrayList<RestaurantReview> reviews, String tripAdvisorURL, String yelpUrl) {
        this.name = name;
        this.address = address;
        this.addressExtended = addressExtended;
        this.locality = locality;
        this.region = region;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.telephone = telephone;
        this.fax = fax;
        this.chainName = chainName;
        this.website = website;
        this.email = email;
        this.cuisine = cuisine;
        this.price = price;
        this.rating = rating;
        this.hours = hours;
        this.parking = parking;
        this.parkingValet = parkingValet;
        this.parkingGarage = parkingGarage;
        this.parkingStreet = parkingStreet;
        this.parkingLot = parkingLot;
        this.parkingValidated = parkingValidated;
        this.parkingFree = parkingFree;
        this.smoking = smoking;
        this.wheelchairAccessible = wheelchairAccessible;
        this.wifi = wifi;
        this.mealBreakfast = mealBreakfast;
        this.mealDeliver = mealDeliver;
        this.mealDinner = mealDinner;
        this.mealLunch = mealLunch;
        this.mealTakeout = mealTakeout;
        this.mealCater = mealCater;
        this.healthyOptions = healthyOptions;
        this.organicOptions = organicOptions;
        this.vegetarianOptions = vegetarianOptions;
        this.veganOptions = veganOptions;
        this.glutenFreeOptions = glutenFreeOptions;
        this.lowFatOptions = lowFatOptions;
        this.reviews = reviews;
        this.tripAdvisorURL = tripAdvisorURL;
        this.yelpUrl = yelpUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressExtended() {
        return addressExtended;
    }

    public void setAddressExtended(String addressExtended) {
        this.addressExtended = addressExtended;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getChainName() {
        return chainName;
    }

    public void setChainName(String chainName) {
        this.chainName = chainName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<String> getCuisine() {
        return cuisine;
    }

    public void setCuisine(ArrayList<String> cuisine) {
        this.cuisine = cuisine;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public RestaurantsDays getHours() {
        return hours;
    }

    public void setHours(RestaurantsDays hours) {
        this.hours = hours;
    }

    public boolean isParking() {
        return parking;
    }

    public void setParking(boolean parking) {
        this.parking = parking;
    }

    public boolean isParkingValet() {
        return parkingValet;
    }

    public void setParkingValet(boolean parkingValet) {
        this.parkingValet = parkingValet;
    }

    public boolean isParkingGarage() {
        return parkingGarage;
    }

    public void setParkingGarage(boolean parkingGarage) {
        this.parkingGarage = parkingGarage;
    }

    public boolean isParkingStreet() {
        return parkingStreet;
    }

    public void setParkingStreet(boolean parkingStreet) {
        this.parkingStreet = parkingStreet;
    }

    public boolean isParkingLot() {
        return parkingLot;
    }

    public void setParkingLot(boolean parkingLot) {
        this.parkingLot = parkingLot;
    }

    public boolean isParkingValidated() {
        return parkingValidated;
    }

    public void setParkingValidated(boolean parkingValidated) {
        this.parkingValidated = parkingValidated;
    }

    public boolean isParkingFree() {
        return parkingFree;
    }

    public void setParkingFree(boolean parkingFree) {
        this.parkingFree = parkingFree;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }

    public boolean isWheelchairAccessible() {
        return wheelchairAccessible;
    }

    public void setWheelchairAccessible(boolean wheelchairAccessible) {
        this.wheelchairAccessible = wheelchairAccessible;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public boolean isMealBreakfast() {
        return mealBreakfast;
    }

    public void setMealBreakfast(boolean mealBreakfast) {
        this.mealBreakfast = mealBreakfast;
    }

    public boolean isMealDeliver() {
        return mealDeliver;
    }

    public void setMealDeliver(boolean mealDeliver) {
        this.mealDeliver = mealDeliver;
    }

    public boolean isMealDinner() {
        return mealDinner;
    }

    public void setMealDinner(boolean mealDinner) {
        this.mealDinner = mealDinner;
    }

    public boolean isMealLunch() {
        return mealLunch;
    }

    public void setMealLunch(boolean mealLunch) {
        this.mealLunch = mealLunch;
    }

    public boolean isMealTakeout() {
        return mealTakeout;
    }

    public void setMealTakeout(boolean mealTakeout) {
        this.mealTakeout = mealTakeout;
    }

    public boolean isMealCater() {
        return mealCater;
    }

    public void setMealCater(boolean mealCater) {
        this.mealCater = mealCater;
    }

    public boolean isHealthyOptions() {
        return healthyOptions;
    }

    public void setHealthyOptions(boolean healthyOptions) {
        this.healthyOptions = healthyOptions;
    }

    public boolean isOrganicOptions() {
        return organicOptions;
    }

    public void setOrganicOptions(boolean organicOptions) {
        this.organicOptions = organicOptions;
    }

    public boolean isVegetarianOptions() {
        return vegetarianOptions;
    }

    public void setVegetarianOptions(boolean vegetarianOptions) {
        this.vegetarianOptions = vegetarianOptions;
    }

    public boolean isVeganOptions() {
        return veganOptions;
    }

    public void setVeganOptions(boolean veganOptions) {
        this.veganOptions = veganOptions;
    }

    public boolean isGlutenFreeOptions() {
        return glutenFreeOptions;
    }

    public void setGlutenFreeOptions(boolean glutenFreeOptions) {
        this.glutenFreeOptions = glutenFreeOptions;
    }

    public boolean isLowFatOptions() {
        return lowFatOptions;
    }

    public void setLowFatOptions(boolean lowFatOptions) {
        this.lowFatOptions = lowFatOptions;
    }

    public ArrayList<RestaurantReview> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<RestaurantReview> reviews) {
        this.reviews = reviews;
    }

    public String getTripAdvisorURL() {
        return tripAdvisorURL;
    }

    public void setTripAdvisorURL(String tripAdvisorURL) {
        this.tripAdvisorURL = tripAdvisorURL;
    }

    public String getYelpUrl() {
        return yelpUrl;
    }

    public void setYelpUrl(String yelpUrl) {
        this.yelpUrl = yelpUrl;
    }

}
