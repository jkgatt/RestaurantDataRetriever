package com.jkg.RestaurantDataRetriever;

import com.jaunt.Element;
import com.jaunt.Elements;
import com.jaunt.NotFound;
import com.jaunt.ResponseException;
import com.jaunt.UserAgent;
import com.jkg.Pojo.RestaurantReview;
import com.jkg.Pojo.RestaurantReviewsWebsite;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author jeangatt
 */
public class ReviewWebScraper {

    private static SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private static Scanner scanner = new Scanner(System.in);

    public static String retrieveTripAdvisorUrl(String restaurantName) throws ResponseException, UnsupportedEncodingException, NotFound {
        //Retrieving Trip Advisor Page
        String[] seperatedRestaurantName = restaurantName.split(" ");
        String finalQueryTerm = String.format("TripAdvisor.com %s", String.join(" ", seperatedRestaurantName));
        String searchUrl = String.format("http://www.bing.com/search?q=%s", URLEncoder.encode(finalQueryTerm, "UTF-8"));
        UserAgent search = new UserAgent();
        search.visit(searchUrl);
        Elements searchResults = search.doc.findEvery("<li class=\"b_algo\"");
        String tripAdvisorUrl = null;
        for (Element searchResult : searchResults) {
            String possibleUrl = searchResult.findFirst("<a href h>").getAt("href");
            if (possibleUrl.contains("www.tripadvisor.com/Restaurant_Review") || possibleUrl.contains("www.tripadvisor.co.uk/Restaurant_Review")) {
                tripAdvisorUrl = possibleUrl;
                break;
            }
        }
        if (tripAdvisorUrl == null) {
            do {
                System.out.println(String.format("Counldn't find trip advisor page for the following restaurant -> %s, please input it manually,", restaurantName));
                tripAdvisorUrl = scanner.next();
            } while (tripAdvisorUrl == null);
        }

        return tripAdvisorUrl;
    }

    public static ArrayList<RestaurantReview> retrieveTripAdvisorReviews(String tripAdvisorUrl) throws NotFound, MalformedURLException, ParseException, InterruptedException, ResponseException, UnsupportedEncodingException {
        ArrayList<RestaurantReview> reviewsList = new ArrayList<>();
        String orignalURL = tripAdvisorUrl;
        URL urlParts = new URL(tripAdvisorUrl);
        System.out.println(String.format("Visting the following website -> %s", tripAdvisorUrl));
        int currentPage = 1;
        int maxPage = 10;
        int urlPageNumber = 0;
        boolean noMoreReviews = false;
        try {
            while (currentPage <= maxPage && !noMoreReviews) {
                UserAgent mainRestaurantPage = new UserAgent();
                if (currentPage > 1) {
                    String[] pathParts = urlParts.getPath().split("-");
                    pathParts[3] = String.format("Reviews-or%s", urlPageNumber);
                    String newPath = String.join("-", pathParts);
                    tripAdvisorUrl = String.format("%s://%s%s", urlParts.getProtocol(), urlParts.getHost(), newPath);
                }
                mainRestaurantPage.visit(tripAdvisorUrl);
                if (currentPage > 1 && orignalURL.equals(mainRestaurantPage.getLocation())) {
                    noMoreReviews = true;
                } else {
                    Elements reviewsDiv = mainRestaurantPage.doc.findEvery("<div id=\"REVIEWS\">");
                    Elements reviews = reviewsDiv.findEvery("<div id=\"review_\\d*\">");

                    for (Element review : reviews) {
                        String singleReviewUrl = review.findFirst("<a href=\"https://www.tripadvisor.com\\/ShowUserReviews[-\\w]*.html#REVIEWS").getAt("href");
                        UserAgent singleReviewRestaurantPage = new UserAgent();
                        singleReviewRestaurantPage.visit(singleReviewUrl);
                        Element singleReviewDiv = singleReviewRestaurantPage.doc.findFirst("<div id=\"review_\\d*\">");

                        //Cleaning the name out, by removing the quotes around the name
                        String reviewTitle = singleReviewDiv.findFirst("<div property=\"name\"").innerHTML().replaceAll("&#x201c;|&#x201d;", "").trim();

                        //Star Rating
                        String starRatingAltText = review.findFirst("<img alt=\"[12345] of 5 stars\"").getAt("alt");
                        int starRating = Integer.parseInt(starRatingAltText.split(" ")[0]);

                        //Review Text
                        String reviewText = singleReviewDiv.findFirst("<p property=\"reviewBody\"").getText().trim();

                        //Review Date
                        String reviewDate = singleReviewDiv.findFirst("<span class content property=\"datePublished\">").getAt("content");

                        //Add review data to list
                        boolean duplicate = false;
                        reviewsList.add(new RestaurantReview(RestaurantReviewsWebsite.TRIPADIVISOR, singleReviewUrl, reviewTitle, reviewText, starRating, df.parse(reviewDate)));
                    }
                    currentPage++;
                    urlPageNumber += 10;
                    Thread.sleep(2000);
                }
            }
            System.out.println(String.format("Retrieved a total of -> %s reviews"
                    + " from the following link -> %s", reviewsList.size(), tripAdvisorUrl));
        } catch (Exception ex) {
            System.out.println(String.format("The following error has occurred-> %s, "
                    + "skiping this restaurant.", ex.getMessage()));
        }
        return reviewsList;
    }
}
