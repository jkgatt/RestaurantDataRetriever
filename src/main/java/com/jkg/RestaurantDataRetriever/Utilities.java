package com.jkg.RestaurantDataRetriever;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jkg.Pojo.Restaurant;
import com.jkg.Pojo.RestaurantsData;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 *
 * @author jeangatt
 */
public class Utilities {
    
    private static Gson gson;
    
    public static String readFromFile(String fileName){
        StringBuilder sb = new StringBuilder();
        try(FileReader fr = new FileReader(fileName)){
            try(BufferedReader br = new BufferedReader(fr)){
                String line;
                while((line = br.readLine()) != null){
                    sb.append(line);
                }
            }
        }catch(Exception ex){
            System.err.println(ex);
        }
        return sb.toString();
    }
    
    public static void saveRestaurantsToFile(String fileName, ArrayList<Restaurant> restaurantInfo){
        try(FileWriter fw = new FileWriter(fileName)){
            gson = new GsonBuilder().create();
            gson.toJson(restaurantInfo, fw);
        }catch(Exception ex){
            System.err.println(ex);
        }
    }
    
    public static void saveRestaurantsDataToFile(String fileName, RestaurantsData restaurantData){
        try(FileWriter fw = new FileWriter(fileName)){
            gson = new Gson();
            gson.toJson(restaurantData, fw);
        }catch(Exception ex){
            System.err.println(ex);
        }
    }
}
