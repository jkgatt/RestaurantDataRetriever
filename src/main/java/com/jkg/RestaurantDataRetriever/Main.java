package com.jkg.RestaurantDataRetriever;

import com.factual.driver.Factual;
import com.factual.driver.FactualApiException;
import com.factual.driver.Query;
import com.google.gson.Gson;
import com.jaunt.NotFound;
import com.jaunt.ResponseException;
import com.jkg.Pojo.Restaurants;
import com.jkg.Pojo.Restaurant;
import com.jkg.Pojo.RestaurantReviewsWebsite;
import com.jkg.Pojo.RestaurantsData;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 *
 * @author jeangatt
 */
public class Main {

    private static final String key = "DFkOsMIww1n6MK2YxIRtkYmMZgaT9IIghzUhOWJx";
    private static final String secret = "43vJwu5kPkNycbwYo93nngcUIt7urmMQpNn1lt9T";
    private static final Factual factual = new Factual(key, secret);
    private static final Gson gson = new Gson();
    
    private static Restaurants restaurantsData;
    private static ArrayList<Restaurant> restaurantInfo;
    private static RestaurantsData restaurantInfoAndReviews;

    public static void main(String[] args) throws Exception {
        //retrieveRestaurantInfoFactual();
        //restaurantInfo = gson.fromJson(Utilities.readFromFile("data/factual/factual_trip_advisor_url_restaurant_data.json"), RestaurantsData.class).getRestaurantList();
        //retrieveReviewsFromUrl(RestaurantReviewsWebsite.TRIPADIVISOR);

        readCleanAndJoinRestaurantReviews();
    }

    private static void retrieveRestaurantInfoFactual() {
        Query query = new Query();
        //filters
        query.field("region").isEqual("CA");
        query.field("locality").isEqual("San Francisco");
        query.field("tel").notBlank();
        query.field("name").notBlank();
        query.field("accessible_wheelchair").notBlank();
        query.field("wifi").notBlank();
        query.field("parking").notBlank();
        query.field("options_healthy").notBlank();
        query.field("options_vegetarian").notBlank();
        query.field("meal_breakfast").notBlank();
        query.field("meal_dinner").notBlank();
        query.field("meal_takeout").notBlank();
        query.field("rating").notBlank();
        query.field("website").notBlank();

        //sort in name ascending order
        query.sortAsc("name");

        //Coloumns to include
        query.only("name", "address", "address_extended", "locality", "region",
                "postcode", "country", "latitude", "longitude", "tel", "fax",
                "chain_name", "website", "email", "cuisine", "price", "rating",
                "hours", "parking", "parking_valet", "parking_garage", "parking_street",
                "parking_lot", "parking_validated", "parking_free", "smoking",
                "meal_breakfast", "meal_lunch", "meal_dinner", "meal_deliver",
                "meal_takeout", "meal_cater", "accessible_wheelchair", "wifi",
                "options_vegetarian", "options_vegan", "options_glutenfree",
                "options_lowfat", "options_organic", "options_healthy");
        int limit = 50;
        query.limit(limit);
        int offset = 0;
        String status = "ok";
        boolean restaurantDataInit = false;
        do {
            query.offset(offset);
            String jsonData = null;
            try {
                jsonData = factual.fetch("restaurants-us", query).getJson();
            } catch (FactualApiException e) {
                status = "error";
                System.out.println("Requested URL: " + e.getRequestUrl());
                System.out.println("Error Status Code: " + e.getStatusCode());
                System.out.println("Error Response Message: " + e.getStatusMessage());
            }
            if (status.equals("ok") && jsonData != null) {
                Restaurants response = gson.fromJson(jsonData, Restaurants.class);
                if (!restaurantDataInit) {
                    restaurantsData = response;
                    restaurantDataInit = true;
                } else {
                    for (Restaurant restaurant : response.getResponse().getRestaurantList()) {
                        restaurantsData.getResponse().getRestaurantList().add(restaurant);
                    }
                }
            }
            offset += limit;
        } while (status.equals("ok") && offset <= 500);
        restaurantInfo = restaurantsData.getResponse().getRestaurantList();
        System.out.println(String.format("Successfully retrieved -> %s records, up to an offset of %s", restaurantInfo.size(), offset));
        Utilities.saveRestaurantsToFile(String.format("%s/%s/%s", "data", "factual", "factual_restaurant_data.json"), restaurantInfo);
    }

    private static void retrieveTripAdvisorUrlForReviews() throws ResponseException, UnsupportedEncodingException, NotFound {
        for (Restaurant restaurant : restaurantInfo) {
            restaurant.setTripAdvisorURL(ReviewWebScraper.retrieveTripAdvisorUrl(restaurant.getName()));
        }
        Utilities.saveRestaurantsToFile(String.format("%s/%s/%s", "data", "factual", "factual_trip_advisor_url_restaurant_data.json"), restaurantInfo);
    }

    private static void retrieveReviewsFromUrl(RestaurantReviewsWebsite reviewWebsite) throws Exception {
        if (reviewWebsite.equals(RestaurantReviewsWebsite.TRIPADIVISOR)) {
            int counter = 1;
            for (Restaurant restaurant : restaurantInfo) {
                if (!restaurant.getTripAdvisorURL().trim().equals("N/A")) {
                    restaurant.setReviews(ReviewWebScraper.retrieveTripAdvisorReviews(restaurant.getTripAdvisorURL()));
                    counter++;
                    if (counter % 10 == 0) {
                        Utilities.saveRestaurantsToFile(String.format("%s/%s/%s", "data", "reviews", "factual_tripadvisor_restaurant_data_" + counter + ".json"), restaurantInfo);
                    }
                }
            }
            ArrayList<Restaurant> restaurantsToRemove = new ArrayList<>();
            for (Restaurant restaurant : restaurantInfo) {
                if (restaurant.getReviews() != null && restaurant.getReviews().size() < 20) {
                    restaurantsToRemove.add(restaurant);
                }
            }
            restaurantInfo.removeAll(restaurantsToRemove);
            int restaurantCount = restaurantInfo.size();
            int reviewCount = 0;
            for (Restaurant restaurant : restaurantInfo) {
                reviewCount += restaurant.getReviews().size();
            }
            restaurantInfoAndReviews = new RestaurantsData(restaurantInfo, restaurantCount, reviewCount);
            Utilities.saveRestaurantsDataToFile(String.format("%s/%s/%s", "data", "reviews", "factual_tripadvisor_restaurant_data_all.json"), restaurantInfoAndReviews);
        }
    }

    private static void readCleanAndJoinRestaurantReviews() {
        
        String fileData = Utilities.readFromFile("data/reviews/factual_tripadvisor_restaurant_data_480test.json");
        RestaurantsData restaurantsData = gson.fromJson(fileData, RestaurantsData.class);
        
        ArrayList<Restaurant> restaurantsToRemove = new ArrayList<>();
        for (Restaurant restaurant : restaurantsData.getRestaurantList()) {
            if (restaurant.getReviews() == null) {
                restaurantsToRemove.add(restaurant);
            }else if(restaurant.getReviews().size() < 20) {
                restaurantsToRemove.add(restaurant);
            }
        }
        restaurantsData.getRestaurantList().removeAll(restaurantsToRemove);
        restaurantsData.setRestaurantCount(restaurantsData.getRestaurantList().size());
        int reviewCount = 0;
        for (Restaurant restaurant : restaurantsData.getRestaurantList()) {
            reviewCount += restaurant.getReviews().size();
        }
        restaurantsData.setTotalReviewCount(reviewCount);
        Utilities.saveRestaurantsDataToFile(String.format("%s/%s/%s/%s", "data", "reviews", "final", "factual_tripadvisor_restaurant_data_all_greater_than_20_reviews.json"), restaurantsData);
    }
}
